﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class MathUtilTestScript
    {
        [Test]
        public void AngularAccelerationWithTorque()
        {
            float acceleration = MathUtil.AngularAccelerationWithTorque(0.25f, 4f, 50f);

            Assert.AreEqual(200, acceleration); // rads per second squared
        }

        [Test]
        public void WheelForce()
        {
            float acceleration = MathUtil.WheelForce(1082.3745F, 0.33965F);

            Assert.AreEqual(3186.7349F, acceleration); // rads per second squared
        }

        [Test]
        public void CalculateFrictionForce()
        {
            float staticFriction = 0.6f;
            float kineticFriction = 0.55f;

            float force = 100F; // newtons
            float load = 49F;

            float frictionForce = MathUtil.CalculateFrictionForce(force, load, staticFriction, kineticFriction);

            Assert.AreEqual(26.95F, frictionForce);
        }
    }
}
