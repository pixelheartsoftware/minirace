﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using static UnityEngine.InputSystem.InputAction;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class PlayerControls : MonoBehaviour
{
    /*
    private Inputs inputs;

    private Rigidbody2D myRigidbody;

    private Vector2 direction = new Vector2(0f, 1f);

    public Wheel wheelFL;
    public Wheel wheelFR;
    public Wheel wheelRL;
    public Wheel wheelRR;

    public float steeringAngle = 0;
    public float slipRatio = 0;

    public float carWidth;
    public float carLength;

    private float carWeight;

    public float currentRevs;
    public EngineModel engineModel;

    public int currentGear = 1;
    public GearboxModel gearboxModel;

    public float wheelToRoadFrictionStatic = 0.6f;
    public float wheelToRoadFrictionKinetic = 0.4f;

    Vector2 previousAcceleration = Vector2.zero;

    float previousRevs = 0F;

    float currentSteeringAngle = 0;

    void Awake()
    {
        inputs = new Inputs();

        inputs.Player.GearUp.performed += context => {
            currentGear = Mathf.Clamp(currentGear + 1, -1, 5);
        };
        inputs.Player.GearDown.performed += context => {
            currentGear = Mathf.Clamp(currentGear - 1, -1, 5);
        };
    }

    private void OnEnable()
    {
        inputs.Enable();
    }

    private void OnDisable()
    {
        inputs.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();

        carLength = Mathf.Abs(wheelFL.transform.position.y - wheelRL.transform.position.y);
        carWidth = Mathf.Abs(wheelFL.transform.position.x - wheelFR.transform.position.x);

        carWeight = myRigidbody.mass;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float wheelLoad = carWeight * 9.8F / 4; // mass times gravitational acceleration, in Newtons;

        float rotationalAcceleration = (((currentRevs - previousRevs) * Mathf.PI) / (Time.deltaTime * 30)) ; // in radians per second^2

        //float torque = gearboxModel.GetAxleTorque(currentRevs, currentGear);

        float torque = engineModel.GetTorque(currentRevs);
        float tractiveForce = CalculateTractiveForce(torque, 0.25F);
        Debug.Log("currentRevs: " + currentRevs + " torque: " + torque + " tractiveForce: " + tractiveForce);

        //float rearWheelForceMagn = 2 * MathUtil.CalculateRearWheelForce(torque, 0.3F, wheelLoad, wheelToRoadFrictionStatic, wheelToRoadFrictionKinetic);

        //All calculations in LOCAL SPACE!!! And local space is the car's space.

        //Vector2 rearWheelForce = new Vector2(0f, rearWheelForceMagn); // newtons of total rear force.
        Vector2 rearWheelForce = new Vector2(0f, tractiveForce); // newtons of total rear force.

        //myRigidbody.AddRelativeForce(rearWheelForce); // simpler than adding two forces to both axles

        myRigidbody.AddForceAtPosition(wheelRL.transform.rotation * (rearWheelForce/2), wheelRL.transform.position);
        myRigidbody.AddForceAtPosition(wheelRR.transform.rotation * (rearWheelForce/2), wheelRR.transform.position);

        // calculate inertia
        Vector2 inertialForce = -CalculateInertia() / 4;

        CalculateAndAddFrontAxleForce(wheelLoad, rearWheelForce, wheelFL, true);

        //if (forceMagn < 0.01F) 
        //{
        //    CalculateAndAddFrontAxleForce(wheelLoad, inertialForce, wheelFL, true);
        //}

        CalculateAndAddFrontAxleForce(wheelLoad, rearWheelForce, wheelFR, true);
        //if (forceMagn < 0.01F)
        //{
        //    CalculateAndAddFrontAxleForce(wheelLoad, inertialForce, wheelFR, true);
        //}
    }

    private Vector2 CalculateInertia()
    {
        Vector2 acceleration = (myRigidbody.velocity - previousAcceleration) / Time.deltaTime;
        Vector2 inertialForce = acceleration * carWeight;

        DebugForce(transform, inertialForce, Color.yellow);

        return inertialForce;
    }

    private void CalculateAndAddFrontAxleForce(float wheelLoad, Vector2 rearWheelForce, Wheel wheel, bool justFriction = false)
    {
        Quaternion wheelLocalRotation = wheel.transform.localRotation.normalized;
        Quaternion wheelWorldRotation = wheel.transform.rotation.normalized;

        Vector2 forceLocal = wheelLocalRotation  * (rearWheelForce / 2);

        DebugForce(transform, transform.rotation * rearWheelForce, Color.red);
        DebugForce(wheel.transform, transform.rotation * forceLocal, Color.red);

        float frictionForceX = MathUtil.CalculateFrictionForce(forceLocal.x, wheelLoad, 1f, 0.6f);
        float frictionForceY = MathUtil.CalculateFrictionForce(forceLocal.y, wheelLoad, 0.014f, 0.01f);

        Vector2 frictionForce = new Vector2(frictionForceX, frictionForceY);

        Vector2 totalForce;
        if (justFriction)
        {
            totalForce = frictionForce;
        } else
        {
            totalForce = rearWheelForce - frictionForce;
        }

        DebugForce(wheel.transform, transform.rotation * totalForce, Color.green);

        //DebugForce(wheel.transform, wheelWorldRotation * new Vector2(frictionForceX, 0), Color.red);
        //DebugForce(wheel.transform, wheelWorldRotation * new Vector2(0, frictionForceY), Color.red);

        myRigidbody.AddForceAtPosition(wheelWorldRotation * totalForce, wheel.transform.position);
    }

    void DebugForce(Transform body, Vector2 force, Color color)
    {
        Debug.DrawLine(body.position, (Vector2)body.position + (force * 0.01F), color);
    }

    float CalculateTorqueAtDriveshaft(
        float torqueAtTheClutch,
        float engineMomentOfInertia = 1.0F // kg * m^2
        )
    {
        float gearRatio = gearboxModel.GetRatio(currentGear);
        float gearRotationalInertia = gearboxModel.GetGearInertia(currentGear);

        return (torqueAtTheClutch - gearRotationalInertia * engineMomentOfInertia) * gearRatio;
    }

    float CalculateTorqueAtTheClutch(
        float engineTorque, // N * m
        float engineRotationalAcceleration, // radians per second^2
        float engineMomentOfInertia = 1.0F // kg * m^2
        )
    {
        return engineTorque - engineRotationalAcceleration * engineMomentOfInertia;
    }

    float CalculateTractiveForce(
        float engineTorque, // N * m
        float wheelRadius = 0.25f
        )
    {
        float gearRatio = gearboxModel.GetRatio(currentGear);

        return (engineTorque * gearRatio) / wheelRadius;
    }

    private void Update()
    {
        float accelerator = inputs.Player.Gas.ReadValue<float>();
        float brake = inputs.Player.Brake.ReadValue<float>();

        if (accelerator > 0F)
        {
            float newRevs = accelerator * 500;
            currentRevs = engineModel.ChangeRevs(newRevs);
        } else
        {
            currentRevs = engineModel.ChangeRevs(Mathf.Lerp(currentRevs, engineModel.dieRevs, 0.2F * Time.deltaTime));
        }

        //steering:
        //float targetSteeringAngle = -30;// inputs.Player.SteeringWheel.ReadValue<float>()  * 30F; // in degrees
        float targetSteeringAngle = inputs.Player.SteeringWheel.ReadValue<float>()  * 60F; // in degrees

        //float currentAngle = MathUtil.AckermanSteeringAngle(carWidth, carLength, wheelFR.transform.localEulerAngles.z);

        currentSteeringAngle = Mathf.Lerp(currentSteeringAngle, targetSteeringAngle, 0.1F);

        Vector2 angles = MathUtil.Ackerman(carWidth, carLength, currentSteeringAngle);

        wheelFR.transform.localRotation = Quaternion.Euler(0, 0, -angles.y);
        wheelFL.transform.localRotation = Quaternion.Euler(0, 0, -angles.x);

        Debug.DrawRay(wheelFR.transform.position, (wheelFR.transform.rotation * Quaternion.Euler(0f, 0f, -45f)) * Vector3.one * 100);
        Debug.DrawRay(wheelFL.transform.position, (wheelFL.transform.rotation * Quaternion.Euler(0f, 0f, -45f)) * Vector3.one * 100);
        Debug.DrawRay(wheelRR.transform.position, (wheelRR.transform.rotation * Quaternion.Euler(0f, 0f, -45f)) * Vector3.one * 100);

    }
    */
}
