﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[CustomEditor(typeof(TireModel))]
public class TireModelEditor : Editor
{
    TireModel model;

    public void OnEnable()
    {
        model = (TireModel)target;

        if (model.Fy.Length == 0)
        {
            model.Fy = new float[8];
            EditorUtility.SetDirty(model);
        }

        if (model.Mz.Length == 0)
        {
            model.Mz = new float[8];
            EditorUtility.SetDirty(model);
        }

        if (model.Fx.Length == 0)
        {
            model.Fx = new float[8];
            EditorUtility.SetDirty(model);
        }
    }

    public override void OnInspectorGUI()
    {
        GUILayout.Label("Lateral force (Fy):");
        model.Fy[0] = EditorGUILayout.FloatField("a1", model.Fy[0]);
        model.Fy[1] = EditorGUILayout.FloatField("a2", model.Fy[1]);
        model.Fy[2] = EditorGUILayout.FloatField("a3", model.Fy[2]);
        model.Fy[3] = EditorGUILayout.FloatField("a4", model.Fy[3]);
        model.Fy[4] = EditorGUILayout.FloatField("a5", model.Fy[4]);
        model.Fy[5] = EditorGUILayout.FloatField("a6", model.Fy[5]);
        model.Fy[6] = EditorGUILayout.FloatField("a7", model.Fy[6]);
        model.Fy[7] = EditorGUILayout.FloatField("a8", model.Fy[7]);

        GUILayout.Label("Aligning Moment (Mz):");
        model.Mz[0] = EditorGUILayout.FloatField("a1", model.Mz[0]);
        model.Mz[1] = EditorGUILayout.FloatField("a2", model.Mz[1]);
        model.Mz[2] = EditorGUILayout.FloatField("a3", model.Mz[2]);
        model.Mz[3] = EditorGUILayout.FloatField("a4", model.Mz[3]);
        model.Mz[4] = EditorGUILayout.FloatField("a5", model.Mz[4]);
        model.Mz[5] = EditorGUILayout.FloatField("a6", model.Mz[5]);
        model.Mz[6] = EditorGUILayout.FloatField("a7", model.Mz[6]);
        model.Mz[7] = EditorGUILayout.FloatField("a8", model.Mz[7]);

        GUILayout.Label("Longitudinal force (Fx):");
        model.Fx[0] = EditorGUILayout.FloatField("a1", model.Fx[0]);
        model.Fx[1] = EditorGUILayout.FloatField("a2", model.Fx[1]);
        model.Fx[2] = EditorGUILayout.FloatField("a3", model.Fx[2]);
        model.Fx[3] = EditorGUILayout.FloatField("a4", model.Fx[3]);
        model.Fx[4] = EditorGUILayout.FloatField("a5", model.Fx[4]);
        model.Fx[5] = EditorGUILayout.FloatField("a6", model.Fx[5]);
        model.Fx[6] = EditorGUILayout.FloatField("a7", model.Fx[6]);
        model.Fx[7] = EditorGUILayout.FloatField("a8", model.Fx[7]);

        model.mass = EditorGUILayout.FloatField("Mass:", model.mass);

        EditorUtility.SetDirty(model);
    }
}
