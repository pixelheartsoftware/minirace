﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[CustomEditor(typeof(SemiEmpiricalEngineModel))]
public class SemiEmpiricalEngineModelEditor : Editor
{
    SemiEmpiricalEngineModel model;

    public void OnEnable()
    {
        model = (SemiEmpiricalEngineModel)target;
    }

    public override void OnInspectorGUI()
    {
        model.mass = EditorGUILayout.FloatField("Mass:", model.mass);

        model.a0 = EditorGUILayout.FloatField("A0:", model.a0);
        model.a1 = EditorGUILayout.FloatField("A1:", model.a1);
        model.a2 = EditorGUILayout.FloatField("A2:", model.a2);

        EditorUtility.SetDirty(model);
    }
}
