﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using static UnityEngine.InputSystem.InputAction;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class PlayerControlsPacejka : MonoBehaviour
{
    /*
    private Inputs inputs;

    private Rigidbody2D myRigidbody;

    private Vector2 direction = new Vector2(0f, 1f);

    public Wheel wheelFrontLeft;
    public Wheel wheelFrontRight;
    public Wheel wheelRearLeft;
    public Wheel wheelRearRight;

    public float steeringAngle = 0;
    public float slipRatio = 0;

    public float carWidth;
    public float carLength;

    private float carWeight;

    private Rigidbody2D wheelAxleFL;
    private Rigidbody2D wheelAxleFR;
    private Rigidbody2D wheelAxleRL;
    private Rigidbody2D wheelAxleRR;

    public float currentRevs;
    public EngineModel engineModel;

    public int currentGear = 1;
    public GearboxModel gearboxModel;

    public float wheelToRoadFrictionStatic = 0.6f;
    public float wheelToRoadFrictionkinetic = 0.4f;

    void Awake()
    {
        inputs = new Inputs();
    }

    private void OnEnable()
    {
        inputs.Enable();
    }

    private void OnDisable()
    {
        inputs.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();

        carLength = Mathf.Abs(wheelFrontLeft.transform.position.y - wheelRearLeft.transform.position.y);
        carWidth = Mathf.Abs(wheelFrontLeft.transform.position.x - wheelFrontRight.transform.position.x);

        carWeight = myRigidbody.mass;

        wheelAxleFL = CreateAxle(wheelFrontLeft);
        wheelAxleFR = CreateAxle(wheelFrontRight);
        wheelAxleRL = CreateAxle(wheelRearLeft);
        wheelAxleRR = CreateAxle(wheelRearRight);
    }

    private Rigidbody2D CreateAxle(Wheel wheel)
    {
        Rigidbody2D wheelAxle = new GameObject().AddComponent<Rigidbody2D>();
        wheelAxle.transform.position = wheel.transform.position;
        FixedJoint2D joint = wheelAxle.gameObject.AddComponent<FixedJoint2D>();
        joint.connectedBody = myRigidbody;
        wheelAxle.angularDrag = 100;
        return wheelAxle;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float wheelLoad = carWeight * 9.8F / 4; // mass times gravitational acceleration, in Newtons;

        float slipAngleFL = myRigidbody.velocity.magnitude < 0.0001F ? 0F : Vector2.SignedAngle(wheelFrontLeft.GetVelocity(), wheelFrontLeft.transform.rotation * Vector3.up);
        float slipAngleFR = myRigidbody.velocity.magnitude < 0.0001F ? 0F : Vector2.SignedAngle(wheelFrontRight.GetVelocity(), wheelFrontRight.transform.rotation * Vector3.up);
        float slipAngleRL = myRigidbody.velocity.magnitude < 0.0001F ? 0F : Vector2.SignedAngle(wheelRearLeft.GetVelocity(), wheelRearLeft.transform.rotation * Vector3.up);
        float slipAngleRR = myRigidbody.velocity.magnitude < 0.0001F ? 0F : Vector2.SignedAngle(wheelRearRight.GetVelocity(), wheelRearRight.transform.rotation * Vector3.up);

        slipAngleFL = NormalizeSlipAngle(slipAngleFL);
        slipAngleFR = NormalizeSlipAngle(slipAngleFR);
        slipAngleRL = NormalizeSlipAngle(slipAngleRL);
        slipAngleRR = NormalizeSlipAngle(slipAngleRR);

        //float engineTorque = engineModel.GetTorque(currentRevs);
        //float axleTorque = gearboxModel.GetAxleTorque(engineTorque, currentGear);

        //float wheelAcceleration = wheelRearRight.GetAngularAcceleration(axleTorque);

        //wheelRearRight.currentAngularSpeed += wheelAcceleration * Time.deltaTime; // in radians per second

        wheelRearRight.currentAngularSpeed = gearboxModel.GetAxleAngularSpeed(currentGear, currentRevs);

        slipRatio = 1;

        //float slipRatio = this.slipRatio; // TODO : Have to calculate is somehow...


        float camber = 0F;

        Pacejka pacejkaFL = MathUtil.CalculatePacejka(slipAngleFL, 1, camber, wheelLoad, wheelFrontLeft.tireModel);
        Pacejka pacejkaFR = MathUtil.CalculatePacejka(slipAngleFR, 1, camber, wheelLoad, wheelFrontRight.tireModel);
        Pacejka pacejkaRL = MathUtil.CalculatePacejka(slipAngleRL, 1, camber, wheelLoad, wheelRearLeft.tireModel);
        Pacejka pacejkaRR = MathUtil.CalculatePacejka(slipAngleRR, 1, camber, wheelLoad, wheelRearRight.tireModel);

        wheelAxleFL.AddRelativeForce(pacejkaFL.LateralForce / 10F);
        wheelAxleFL.AddRelativeForce(pacejkaFL.LongitudinalForce / 10F);
        wheelAxleFL.AddTorque(pacejkaFL.AligningMoment * 100F);

        wheelAxleFR.AddRelativeForce(pacejkaFR.LateralForce / 10F);
        wheelAxleFR.AddRelativeForce(pacejkaFR.LongitudinalForce / 10F);
        wheelAxleFR.AddTorque(pacejkaFR.AligningMoment * 100F);

        wheelAxleRL.AddRelativeForce(pacejkaRL.LateralForce / 10F);
        wheelAxleRL.AddRelativeForce(pacejkaRL.LongitudinalForce / 10F);
        wheelAxleRL.AddTorque(pacejkaRL.AligningMoment * 100F);

        wheelAxleRR.AddRelativeForce(pacejkaRR.LateralForce / 10F);
        wheelAxleRR.AddRelativeForce(pacejkaRR.LongitudinalForce / 10F);
        wheelAxleRR.AddTorque(pacejkaRR.AligningMoment * 100F);
    }

    private float NormalizeSlipAngle(float angle)
    {
        if (angle > 90F)
        {
            angle = (angle - 180F);
        } else 
        if (angle < -90F)
        {
            angle = (angle + 180F);
        } else
        {
            angle = -angle;
        }
        return angle;
    }

    private void Update()
    {
        float accelerator = inputs.Player.Gas.ReadValue<float>();
        float brake = inputs.Player.Brake.ReadValue<float>();

        if (accelerator > 0F)
        {
            float newRevs = accelerator * Time.deltaTime * 1000;
            currentRevs = engineModel.ChangeRevs(newRevs);
        } else
        {
            currentRevs = engineModel.ChangeRevs(Mathf.Lerp(currentRevs, engineModel.dieRevs, 0.2F * Time.deltaTime));
        }


        //steering:
        float targetSteeringAngle = inputs.Player.SteeringWheel.ReadValue<float>()  * 30F; // in degrees

        float currentAngle = MathUtil.AckermanSteeringAngle(carWidth, carLength, wheelFrontRight.transform.localEulerAngles.z);

        float setAngle = Mathf.Clamp(currentAngle, targetSteeringAngle, 0.5F);

        Vector2 angles = MathUtil.Ackerman(carWidth, carLength, setAngle);

        wheelFrontRight.transform.localRotation = Quaternion.Euler(0, 0, -angles.y);
        wheelFrontLeft.transform.localRotation = Quaternion.Euler(0, 0, -angles.x);



        Debug.DrawRay(wheelFrontRight.transform.position, (wheelFrontRight.transform.rotation * Quaternion.Euler(0f, 0f, -45f)) * Vector3.one * 100);
        Debug.DrawRay(wheelFrontLeft.transform.position, (wheelFrontLeft.transform.rotation * Quaternion.Euler(0f, 0f, -45f)) * Vector3.one * 100);
        Debug.DrawRay(wheelRearRight.transform.position, (wheelRearRight.transform.rotation * Quaternion.Euler(0f, 0f, -45f)) * Vector3.one * 100);

    }
    */
}
