﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using static UnityEngine.InputSystem.InputAction;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class PlayerControlsNew : MonoBehaviour
{
    private Inputs inputs;

    private Rigidbody2D myRigidbody;

    float currentSteeringAngle = 0;

    public int currentGear = 1;

    private float targetThrottle = 0; // 0..1

    public CarModel carModel;

    private CarState carState = new CarState();

    public Wheel wheelFR, wheelFL, wheelRL, wheelRR;

    void Awake()
    {
        inputs = new Inputs();

        inputs.Player.GearUp.performed += context => {
            currentGear = Mathf.Clamp(currentGear + 1, -1, 5);
        };
        inputs.Player.GearDown.performed += context => {
            currentGear = Mathf.Clamp(currentGear - 1, -1, 5);
        };

        carState.Init(carModel);

        
    }

    private void OnEnable()
    {
        inputs.Enable();
    }

    private void OnDisable()
    {
        inputs.Disable();
    }

    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();

        myRigidbody.mass = carModel.GetLumpedMass();
    }

    void FixedUpdate()
    {
        CarStateOutput carOutput = carState.Step(new CarStateInput(targetThrottle, currentSteeringAngle, currentGear, myRigidbody.velocity, Time.deltaTime));

        wheelFR.transform.localRotation = Quaternion.Euler(0, 0, carOutput.steeringOutput.rightAngle);
        wheelFL.transform.localRotation = Quaternion.Euler(0, 0, carOutput.steeringOutput.leftAngle);

        myRigidbody.AddForce(carOutput.force);
        myRigidbody.AddTorque(carOutput.torque);
    }

    void DebugForce(Transform body, Vector2 force, Color color)
    {
        Debug.DrawLine(body.position, (Vector2)body.position + (force * 0.01F), color);
    }

    private void Update()
    {
        // actuation:
        // longitudinal:
        targetThrottle = inputs.Player.Gas.ReadValue<float>();
        float brake = inputs.Player.Brake.ReadValue<float>();

        // lateral:
        //float targetSteeringAngle = -30;// inputs.Player.SteeringWheel.ReadValue<float>()  * 30F; // in degrees
        float targetSteeringAngle = inputs.Player.SteeringWheel.ReadValue<float>()  * 60F; // in degrees

        //float currentAngle = MathUtil.AckermanSteeringAngle(carWidth, carLength, wheelFR.transform.localEulerAngles.z);

        currentSteeringAngle = Mathf.Lerp(currentSteeringAngle, targetSteeringAngle, 0.1F);


    }
}
