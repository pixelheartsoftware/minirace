﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Wheel : MonoBehaviour
{
    private Rigidbody2D myRigidBody;
    public TireModel tireModel;

    public float currentAngularSpeed = 0f;
    public float radius = 0.25f; // meters

    public float momentOfInetria;

    public Vector2 currentVelocity;
    public float currentRotationSpeed;

    public Vector2 rotatedVelocity;

    private Vector2 prevVelocity = Vector2.zero;
    public Vector2 acceleration;
    public Vector2 calculatedForce;

    public double calculatedForceX, calculatedForceY;


    private void Awake()
    {
        myRigidBody = GetComponent<Rigidbody2D>();

        momentOfInetria = 0.5F * myRigidBody.mass * radius * radius;
    }

    private void FixedUpdate()
    {
        acceleration = (myRigidBody.velocity - prevVelocity) * (Time.deltaTime);

        calculatedForce = acceleration * 250;
    }


    public void AddTorque(float torque)
    {
        myRigidBody.AddTorque(torque);
    }

    internal void AddRelativeForce(Vector2 force)
    {
        myRigidBody.AddRelativeForce(Vector3.up * force);

        Debug.DrawRay(transform.position, transform.rotation * force * 0.01F);
    }

    internal Vector3 GetVelocity()
    {
        return myRigidBody.velocity;
    }

    internal float GetContactPatchSpeed()
    {
        return currentAngularSpeed * radius;
    }

    internal float GetAngularAcceleration(float torque)
    {
        return MathUtil.AngularAccelerationWithTorque(radius, myRigidBody.mass, torque);
    }
}