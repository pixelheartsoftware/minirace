﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringState : LumpedState<SteeringModel, SteeringStateInput, SteeringStateOutput>
{
    public override SteeringStateOutput Step(SteeringStateInput input)
    {
        Vector2 angles = MathUtil.Ackerman(input.chasisDimensions.x, input.chasisDimensions.y, input.steeringAngle);

        return new SteeringStateOutput(-angles.x, -angles.y);
    }
}

public class SteeringStateInput : LumpedStateInput
{
    public Vector2 chasisDimensions;
    public float steeringAngle;

    public SteeringStateInput(Vector2 chasisDimensions, float steeringAngle, float deltaTime) : base(deltaTime)
    {
        this.chasisDimensions = chasisDimensions;
        this.steeringAngle = steeringAngle;
    }
}

public class SteeringStateOutput : LumpedStateOutput
{
    public float leftAngle, rightAngle;

    public SteeringStateOutput(float leftAngle, float rightAngle)
    {
        this.rightAngle = rightAngle;
        this.leftAngle = leftAngle;
    }
}