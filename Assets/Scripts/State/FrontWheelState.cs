﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontWheelState : LumpedState<WheelModel, FrontWheelStateInput, FrontWheelStateOutput>
{
    private TireState tire = new TireState();

    public override FrontWheelStateOutput Step(FrontWheelStateInput input)
    {
        FrontWheelStateOutput output = new FrontWheelStateOutput();

        output.angularVelocity = input.forwardVelocity.magnitude / model.radius;

        return output;
    }

    internal float GetRotationalSpeed()
    {
        throw new NotImplementedException();
    }

    public override void Init(WheelModel wheel)
    {
        base.Init(wheel);

        tire.Init(wheel.tire);
    }
}

public class FrontWheelStateInput : LumpedStateInput
{
    public Vector2 forwardVelocity;

    public FrontWheelStateInput(Vector2 forwardVelocity, float deltaTime) : base(deltaTime)
    {
        this.forwardVelocity = forwardVelocity;
    }
}

public class FrontWheelStateOutput : LumpedStateOutput
{
    public float angularVelocity; // radians per second
}
