﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarState : LumpedState<CarModel, CarStateInput, CarStateOutput>
{
    public ChasisState chasis = new ChasisState();

    public EngineState engine = new EngineState();

    public GearboxState gearbox = new GearboxState();

    public SteeringState steering = new SteeringState();

    public FrontWheelState wheelFL = new FrontWheelState(),
                        wheelFR = new FrontWheelState();

    public RearWheelState wheelRL = new RearWheelState(), 
                        wheelRR = new RearWheelState();

    public override void Init(CarModel car)
    {
        base.Init(car);

        chasis.Init(car.chasis);
        engine.Init(car.engine);
        gearbox.Init(car.gearbox);
        steering.Init(car.steering);

        wheelFL.Init(car.wheelFL);
        wheelFR.Init(car.wheelFR);
        wheelRL.Init(car.wheelRL);
        wheelRR.Init(car.wheelRR);
    }

    public override CarStateOutput Step(CarStateInput input)
    {
        SteeringStateOutput steeringOut = steering.Step(new SteeringStateInput(chasis.GetDimensions(), input.steeringAngle, input.deltaTime));

        FrontWheelStateOutput wheelOutFL = wheelFL.Step(new FrontWheelStateInput(input.velocity, input.deltaTime));
        FrontWheelStateOutput wheelOutFR = wheelFR.Step(new FrontWheelStateInput(input.velocity, input.deltaTime));

        // TODO : Pacejka - calculate front wheel lateral and longitudinal forces.

        float driveWheelRotationalSpeed = wheelRL.GetRotationalSpeed();

        EngineStateOutput engineOutput = engine.Step(new EngineStateInput(input.throttlePercent, driveWheelRotationalSpeed, input.deltaTime));

        GearboxStateOutput gearboxOutput = gearbox.Step(new GearboxStateInput(engineOutput.torque, input.deltaTime));

        // TODO: Introduce differential model and state
        RearWheelStateOutput wheelOutRL = wheelRL.Step(new RearWheelStateInput(gearboxOutput.torque / 2, input.deltaTime));
        RearWheelStateOutput wheelOutRR = wheelRR.Step(new RearWheelStateInput(gearboxOutput.torque / 2, input.deltaTime));

        // TODO : Pacejka - calculate rear wheel lateral and longitudinal forces.

        Vector2 totalForce = new Vector2(0F, 100F);
        float totalTorque = 10F;

        return new CarStateOutput(totalForce, totalTorque, steeringOut);
    }
}

public class CarStateInput : LumpedStateInput
{
    public float throttlePercent; //0..1

    public float steeringAngle; // degrees

    public int gear; // gear number, -1 .. 5, where 0 = idle, -1 = reverse

    public Vector2 velocity; // World frame

    public CarStateInput(float throttlePercent, float steeringAngle, int gear, Vector2 velocity, float deltaTime) : base(deltaTime)
    {
        this.throttlePercent = throttlePercent;
        this.steeringAngle = steeringAngle;
        this.gear = gear;
        this.velocity = velocity;
    }
}

public class CarStateOutput : LumpedStateOutput
{
    public Vector2 force;
    public float torque;

    public SteeringStateOutput steeringOutput;

    public CarStateOutput(Vector2 force, float torque, SteeringStateOutput steeringOutput)
    {
        this.force = force;
        this.torque = torque;
        this.steeringOutput = steeringOutput;
    }
}