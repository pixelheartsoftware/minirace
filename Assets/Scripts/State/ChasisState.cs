﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasisState : LumpedState<ChasisModel, ChasisStateInput, ChasisStateOutput>
{
    public override ChasisStateOutput Step(ChasisStateInput input)
    {
        throw new System.NotImplementedException();
    }

    internal Vector2 GetDimensions()
    {
        return model.dimensions;
    }
}

public class ChasisStateInput : LumpedStateInput
{
    public ChasisStateInput(float deltaTime) : base(deltaTime)
    {

    }
}

public class ChasisStateOutput : LumpedStateOutput
{

}