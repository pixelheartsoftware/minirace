﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineState : LumpedState<EngineModel, EngineStateInput, EngineStateOutput>
{
    private float rotationalSpeed; // radians per second

    public override EngineStateOutput Step(EngineStateInput input)
    {
        EngineStateOutput engineStateOutput = new EngineStateOutput();

        engineStateOutput.torque = model.GetTorque(rotationalSpeed, input.throttlePercent);

        Debug.Log("Current rotational speed: " + rotationalSpeed + " throttlePercent: " + input.throttlePercent + " torque: " + engineStateOutput.torque);

        return engineStateOutput;
    }
}

public class EngineStateInput : LumpedStateInput
{
    public float throttlePercent; // fraction

    public float driveWheelsRotationalSpeed; // radians per second

    public EngineStateInput(float throttlePercent, float driveWheelsRotationalSpeed, float deltaTime) : base(deltaTime)
    {
        this.throttlePercent = throttlePercent;
        this.driveWheelsRotationalSpeed = driveWheelsRotationalSpeed;
    }
}

public class EngineStateOutput : LumpedStateOutput
{
    public float torque; // Nm
}
