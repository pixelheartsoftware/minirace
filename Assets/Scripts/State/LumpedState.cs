﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LumpedState<M, I, O> where I : LumpedStateInput where M : LumpedModel where O : LumpedStateOutput
{
    protected M model;

    public abstract O Step(I input);

    public virtual void Init(M model)
    {
        this.model = model;
    }
}

public abstract class LumpedStateInput
{
    public float deltaTime;

    protected LumpedStateInput(float deltaTime)
    {
        this.deltaTime = deltaTime;
    }
}

public abstract class LumpedStateOutput
{

}