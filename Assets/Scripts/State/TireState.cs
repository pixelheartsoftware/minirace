﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TireState : LumpedState<TireModel, TireStateInput, TireStateOutput>
{
    //TODO: Do something here
    public float temperature;

    public override TireStateOutput Step(TireStateInput input)
    {
        throw new System.NotImplementedException();
    }
}

public class TireStateInput : LumpedStateInput
{
    public TireStateInput(float deltaTime) : base(deltaTime)
    {

    }
}

public class TireStateOutput : LumpedStateOutput
{

}