﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearboxState : LumpedState<GearboxModel, GearboxStateInput, GearboxStateOutput>
{
    public override GearboxStateOutput Step(GearboxStateInput input)
    {
        GearboxStateOutput output = new GearboxStateOutput();

        output.torque = input.engineTorque;

        return output;
    }
}

public class GearboxStateInput : LumpedStateInput
{
    public float engineTorque;

    public GearboxStateInput(float engineAngularMomentum, float deltaTime) : base(deltaTime)
    {
        this.engineTorque = engineAngularMomentum;
    }
}

public class GearboxStateOutput : LumpedStateOutput
{
    public float torque;
}