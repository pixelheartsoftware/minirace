﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RearWheelState : LumpedState<WheelModel, RearWheelStateInput, RearWheelStateOutput>
{
    private TireState tire = new TireState();

    private float rotationalSpeed;

    public override RearWheelStateOutput Step(RearWheelStateInput input)
    {
        RearWheelStateOutput output = new RearWheelStateOutput();

        rotationalSpeed += ((input.axleTorque) / (model.mass * input.deltaTime));

        output.angularVelocity = rotationalSpeed;

        return output;
    }

    internal float GetRotationalSpeed()
    {
        return rotationalSpeed;
    }

    public override void Init(WheelModel wheel)
    {
        base.Init(wheel);

        tire.Init(wheel.tire);
    }
}

public class RearWheelStateInput : LumpedStateInput
{
    public float axleTorque;

    public RearWheelStateInput(float axleTorque, float deltaTime) : base(deltaTime)
    {
        this.axleTorque = axleTorque;
    }
}

public class RearWheelStateOutput : LumpedStateOutput
{
    public float angularVelocity;
}
