﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Car", menuName = "Model Elements/Car Model", order = 1)]
[System.Serializable]
public class CarModel : ScriptableObject, LumpedModel
{
    public ChasisModel chasis;

    public EngineModel engine;

    public GearboxModel gearbox;

    public WheelModel wheelFL, wheelFR, wheelRL, wheelRR;

    public SteeringModel steering;

    public float GetLumpedMass()
    {
        return chasis.GetLumpedMass() +
            engine.GetLumpedMass() +
            gearbox.GetLumpedMass() +
            wheelFL.GetLumpedMass() +
            wheelFR.GetLumpedMass() +
            wheelRL.GetLumpedMass() +
            wheelRR.GetLumpedMass() +
            steering.GetLumpedMass();
    }
}
