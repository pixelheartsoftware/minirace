﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Wheel", menuName = "Model Elements/Wheel Model", order = 7)]
[System.Serializable]
public class WheelModel : ScriptableObject, LumpedModel
{
    public TireModel tire;
    public float radius; // meters

    public float mass;

    public float GetLumpedMass()
    {
        return mass + tire.GetLumpedMass();
    }
}
