﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Gearbox", menuName = "Model Elements/Gearbox Model", order = 4)]
[System.Serializable]
public class GearboxModel : ScriptableObject, LumpedModel
{
    public float[] ratios;
    public float reverseRatio;

    public float mass;

    public float GetLumpedMass()
    {
        return mass;
    }
    // ================================
    public float GetAxleTorque(float engineTorque, int gear)
    {
        if (gear == 0)
        {
            return 0;
        }
        if (gear == -1)
        {
            return engineTorque / reverseRatio;
        }

        return engineTorque / ratios[gear - 1];
    }

    internal float GetAxleAngularSpeed(int gear, float currentRevs)
    {
        if (gear == 0)
        {
            return 0;
        }
        if (gear == -1)
        {
            return reverseRatio * currentRevs;
        }
        return currentRevs * ratios[gear];
    }

    internal float GetRatio(int currentGear)
    {
        if (currentGear == 0)
        {
            return 0;
        }
        if (currentGear == -1)
        {
            return 1/reverseRatio;
        }

        return 1/ratios[currentGear - 1];
    }

    internal float GetGearInertia(int currentGear)
    {
        return 1f;
    }
}
