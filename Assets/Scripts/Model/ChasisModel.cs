﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Chasis", menuName = "Model Elements/Chasis Model", order = 2)]
[System.Serializable]
public class ChasisModel : ScriptableObject, LumpedModel
{
    public float mass; // kilograms
    public Vector2 dimensions; // x - width, y - length

    public float GetLumpedMass()
    {
        return mass;
    }
}
