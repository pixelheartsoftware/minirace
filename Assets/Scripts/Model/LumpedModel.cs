﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface LumpedModel
{
    /// <summary>
    /// Returns the total mass ot this model and all lumped elements it contains.
    /// </summary>
    /// <returns>Sum total of all the masses.</returns>
    float GetLumpedMass(); 
}
