﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Engine", menuName = "Model Elements/Engine/Semi-empirical Engine Model", order = 3)]
[System.Serializable]
public class SemiEmpiricalEngineModel : EngineModel
{
    public float a0;
    public float a1;
    public float a2;

    /// <summary>
    /// Returns engine torque for the given parameters.
    /// This implementation returns semi-empirical result based on a polinomial with configurable parameters.
    /// </summary>
    /// <param name="engineRotationalSpeed">Rotational speed in radians per second.</param>
    /// <param name="throttlePosition">Percent (0 to 1 inclusive) of throttle press.</param>
    /// <returns>engine torque in Nm</returns>
    public override float GetTorque(float engineRotationalSpeed, float throttlePosition)
    {
        return throttlePosition * (a0 + a1* engineRotationalSpeed + a2*engineRotationalSpeed*engineRotationalSpeed);
    }
}
