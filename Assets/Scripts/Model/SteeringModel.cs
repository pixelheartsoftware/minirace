﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Steering", menuName = "Model Elements/Steering Model", order = 5)]
[System.Serializable]
public class SteeringModel : ScriptableObject, LumpedModel
{
    public float mass;

    public float GetLumpedMass()
    {
        return mass;
    }
}
