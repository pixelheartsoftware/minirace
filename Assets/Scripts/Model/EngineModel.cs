﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Engine", menuName = "Model Elements/Engine/Engine Model", order = 3)]
[System.Serializable]
public class EngineModel : ScriptableObject, LumpedModel
{
    public float engineTorque;

    public float mass;

    public float GetLumpedMass()
    {
        return mass;
    }

    /// <summary>
    /// Returns engine torque for the given parameters.
    /// </summary>
    /// <param name="engineRotationalSpeed">Rotational speed in radians per second.</param>
    /// <param name="throttlePosition">Percent (0 to 1 inclusive) of throttle press.</param>
    /// <returns>engine torque in Nm</returns>
    public virtual float GetTorque(float engineRotationalSpeed, float throttlePosition)
    {
        return engineTorque;
    }
}
